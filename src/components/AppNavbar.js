import {Link} from 'react-router-dom'
import {useContext} from 'react'
import {Navbar, Container, Nav} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function AppNavbar(){
	
	const {user} = useContext(UserContext);


	return(
		<Navbar className="navBar" expand="lg">
	      	<Container>
		        <Navbar.Brand as={Link} to="/" className="fs-2">E-commerce</Navbar.Brand>
		          <Navbar.Toggle aria-controls="basic-navbar-nav" />
		            <Navbar.Collapse id="basic-navbar-nav">
		             <Nav className="me-auto nav">
		               <Nav.Link as={Link} to="/" className="links">Home</Nav.Link>
		               <Nav.Link as={Link} to="/products" className="links">Products</Nav.Link>
		              {
		              	(user.isAdmin) ?
		              	<Nav.Link as={Link} to="/dashboard">Dashboard</Nav.Link>
		              	:
		              	<Nav.Link hidden></Nav.Link>
		              } 
		              {
		                (user.id !== null) ?
		                <>
		                  <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
		                  <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
		                </>
		                :
		                <>
		                  <Nav.Link as={Link} to="/login" className="links">Login</Nav.Link>
		                  <Nav.Link as={Link} to="/register" className="links">Sign up</Nav.Link>
		                </>
		              }  
		          </Nav>
		        </Navbar.Collapse>
	      	</Container>
    	</Navbar>


		)


}