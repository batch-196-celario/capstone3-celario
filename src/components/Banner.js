import {Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'


export default function Banner(){
	return (
		<>
			<div className="bannerDiv">
				<h1 className="bannerh1">JC's Shopping Center</h1>
				<p>Great deals, Fair Prices, We have a variety of items all across the board</p>
				<Button variant ="danger" as={Link} to="/products">Shop Now</Button>
			</div>
		</>
		)
}