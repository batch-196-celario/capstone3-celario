
import {Button, Col, Container, Card, Row} from "react-bootstrap";
import {Link} from 'react-router-dom'

export default function ProductCard ({productProp}) {
	
	const {name, description,price, _id} = productProp



	return (
		<>
			<Container className="d-grid">
				<Row>
					<Col lg={6} md={6} sm={12}>
						<Card className="p-3 mb-3">
							<Card.Body>
								<Card.Title className="fw-bold my-2">
									{name}
								</Card.Title>
								<Card.Subtitle className="fw-bold text-justify mt-2">Description:</Card.Subtitle>
								<Card.Text className="mt-1">
									{description}
								</Card.Text>
					    		<Card.Subtitle className="fw-bold">Price:</Card.Subtitle>
								<Card.Text className="mt-1">&#8369;{price}</Card.Text>
						{/*<Card.Text>Enrollees: {count}</Card.Text>
						<Card.Text>Seats: {seat}</Card.Text>*/}
						{/*<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
								<Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>						
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		</>

	)
}