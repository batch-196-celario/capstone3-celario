import {useState, useEffect, useContext} from 'react'
import {Container, Row, Col, Card, Button, Modal, Form} from 'react-bootstrap'
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'



export default function ProductView(){
	const {user} = useContext(UserContext);
	const history = useNavigate();

	//for modal
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const {productId} = useParams();

	const purchase = (productId) => {
		fetch(`https://rocky-hollows-82138.herokuapp.com/orders/createOrder`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				totalAmt: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
	}

	// update product experiment
    function update (e) {
    	fetch(`https://rocky-hollows-82138.herokuapp.com/products/updateProduct/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if(data){

                //Clear fields
                setName('');
                setDescription('');
                setPrice('');

                Swal.fire({
                    title: 'Product successfully updated',
                    icon: 'success',
                    text: 'You can add more'
                });
            //history("/products");
            }else{
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please try again later'
                })
            }
        })
    };


	useEffect(() => {
		fetch(`https://rocky-hollows-82138.herokuapp.com/products/SingleProduct/${productId}`)

		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])





	return(
		<>
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body>
							<Card.Title className="text-center">{name}</Card.Title>
							<Card.Subtitle className="text-center mt-3">Description: {description}</Card.Subtitle>
							<Card.Text></Card.Text>
							<Card.Subtitle className="text-center mt-2">Price: &#8369;{price}</Card.Subtitle>
							
							{ user.id !== null ?
								<div className="row d-flex justify-content-center align-content-center">
								<Button variant="primary"
										className ="mt-3" 
										disabled
										onClick={() => purchase(productId)}>Buy</Button>
								</div>
								:
								<Button hidden></Button>
							}
							{ (user.isAdmin) ?
								<div className="row d-flex justify-content-center align-content-center">
								<Button className="mx-3 mt-3" onClick={handleShow}>Update</Button>
								</div>
								:
								<Button hidden></Button>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	{/*Modal*/}
		<Modal show={show} onHide={handleClose}>
        	<Modal.Header closeButton>
          		<Modal.Title>Update Products</Modal.Title>
        	</Modal.Header>
        	<Modal.Body>
        		<Form onSubmit = {(e) => update(e)}>
		            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
		              <Form.Label>Name</Form.Label>
		              <Form.Control
		                type="text"
		                placeholder="updated product name"
		                value={name} 
                        onChange={e => setName(e.target.value)}
		              />
		            </Form.Group>
		            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
		              <Form.Label>Description</Form.Label>
		              <Form.Control
		                type="text"
		                placeholder="updated product Description"
		                value={description} 
                        onChange={e => setDescription(e.target.value)}
		              />
		            </Form.Group>
		            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
		              <Form.Label>Price</Form.Label>
		              <Form.Control
		                type="number"
		                placeholder="updated product price"
		                value={price} 
                        onChange={e => setPrice(e.target.value)}
		              />
		            </Form.Group>
                      <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                      </Button>
          		</Form>
        	</Modal.Body>
      	</Modal>



		</>	
	)
}