import {useState, useEffect, useContext} from 'react'
import {Table, Button, Modal, Form} from 'react-bootstrap'
import Swal from 'sweetalert2'
import {Navigate, useParams, Link} from 'react-router-dom'
import UserContext from "../UserContext";


export default function Admin (){

	//Modal
	const [show, setShow] = useState(false);
	const [showAdd, setShowAdd] = useState(false)
  	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);
  	const Show = () => setShowAdd(true);
  	const Close = () => setShowAdd(false);


	const {user} = useContext(UserContext)
    const {productId} = useParams();

	const [products, setProducts] = useState([])
	const [openEdit, setOpenEdit] = useState(false)

	const [name, setProductName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [isActive, setIsActive] = useState(false);

    // for update
    const [updateName, setUpdateName] = useState("");
    const [updateDescription, setUpdateDescription] = useState("");
    const [updatePrice, setUpdatePrice] = useState("");

	const getData = () =>{
		fetch("https://rocky-hollows-82138.herokuapp.com/products")
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(products => {
				return (
					<tr key={products._id}>
                        <td className="fs-10">{products._id}</td>
                        <td className="fs-10">{products.name}</td>
                        <td className="fs-10">{products.description}</td>
                        <td>&#8369;{products.price}</td>
                        {/*<td>{products.quantity}</td>*/}
                        <td>{products.isActive ? "Active" : "Inactive"}</td>
                        <td>
                            {
                                (products.isActive)
                                ?

                                    <Button variant="danger" onClick ={() => archive(products._id, products.name)}>Archive</Button>
                                :
                                    <>
                                        <Button variant="success" onClick ={() => activate(products._id, products.name)}>Activate</Button>
                                    </>
                            }
                        </td>
                    </tr>
				)
			}))
		})

	}

		function addProduct(e){
        //prevents the page redirection via form submit
        e.preventDefault();

                fetch('https://rocky-hollows-82138.herokuapp.com/products/createProduct', {
                    method: "POST",
                    headers:{
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${localStorage.getItem("token")}`
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price
                        //quantity: quantity,
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        //Clear input fields
                        setProductName("");
                        setDescription("");
                        setPrice("");
                        //setQuantity("");

                        Swal.fire({
                            title: "Completed",
                            icon: "success",
                            text: "Product successfully added!"
                        })
                        getData();
                    }
                    else{
                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
                    }
                })
            }

        useEffect(()=>{
        	if((name !== "" && description !== "" && price !== "")){
            setIsActive(true);
        	}
        	else{
            setIsActive(false);
        	}
    	},[name, description, price])



	   	const archive = (productId, productName) =>{
        console.log(productId);
        console.log(productName);

        fetch(`https://rocky-hollows-82138.herokuapp.com/products/archiveProduct/${productId}`,{
            method: "DELETE",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Archive Succesful!",
                    icon: "success",
                    text: `${productName} is now inactive.`
                })
                getData();
            }
            else{
                Swal.fire({
                    title: "Archive Unsuccessful!",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                })
            }
        })
    }

        const activate = (productId, productName) =>{
        console.log(productId);
        console.log(productName);

        fetch(`https://rocky-hollows-82138.herokuapp.com/products/activateProduct/${productId}`,{
            method: "PUT",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Activate Succesful!",
                    icon: "success",
                    text: `${productName} is now active.`
                })
                getData();
            }
            else{
                Swal.fire({
                    title: "Activate Unsuccessful!",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                })
            }
        })
    }

    useEffect(()=>{

        getData();
    }, [])



	return (
		(!user.isAdmin) ?
        <Navigate to="/"/>
        :
	<>
        <h1 className="text-center">ADMIN DASHBOARD</h1>
		<div className= "d-flex justify-content-center align-content-center">
			<Button onClick={Show}>Add Product</Button>
            <Button className="mx-3" as={Link} to="/products">Update Product</Button>
        </div>
		<div className="mt-5">
		<Table striped bordered hover variant="dark">
      		<thead>
        	<tr>
          		<th>Product ID</th>
          		<th>Product Name</th>
          		<th>Description</th>
          		<th>Price</th>
          		<th>Status</th>
          		<th>Actions</th>
        	</tr>
      		</thead>
      		<tbody>
        		{products}
      		</tbody>
    	</Table>
    	</div>

      	<Modal show={showAdd} onHide={Close}>
        	<Modal.Header closeButton>
          		<Modal.Title>Create a Product</Modal.Title>
        	</Modal.Header>
        	<Modal.Body>
        		<Form  onSubmit = {(e) => addProduct(e)}>
                <Form.Group className="mb-3" controlId="name">
                  <Form.Label>Product Name</Form.Label>
                  <Form.Control type="text" placeholder="Product Name" value={name} onChange={e => setProductName(e.target.value)}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="description">
                  <Form.Label>Product description</Form.Label>
                  <Form.Control type="text" placeholder="description" value={description} onChange={e => setDescription(e.target.value)}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="price">
                  <Form.Label>Price</Form.Label>
                  <Form.Control type="number" placeholder="price" value={price} onChange={e => setPrice(e.target.value)}/>
                </Form.Group>
              {
                  isActive
                  ?
                      <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                      </Button>
                  :
                      <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Submit
                      </Button>
              }
            </Form>
        	</Modal.Body>
      	</Modal>
    </>
	)
}