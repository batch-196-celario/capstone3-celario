import {Link} from 'react-router-dom';
import {Container, Button} from 'react-bootstrap';




export default function Error () {
	return (
		<>
			<Container className="mx-5 mt-5">
				<h1>404 page not found</h1>
				<p>Oooops something went wrong</p>
				<Button variant="primary" as={Link} to="/">back to home</Button>
			</Container>
		</>
	)
}