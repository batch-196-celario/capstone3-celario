import {useState, useEffect, useContext} from 'react'
import {Button, Col, Container, Form, Row} from "react-bootstrap";
import {Navigate} from 'react-router-dom'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';





export default function Login(){
	
    const {user, setUser} = useContext(UserContext);
    console.log(user);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [active, setActive] = useState(false);

    function loginUser(e){
        e.preventDefault();

        fetch('https://rocky-hollows-82138.herokuapp.com/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Succesful",
                    icon: "success",
                    text: `Welcome ${email} !`
                });
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Invalid email or password"
                });
            };
        });

        setEmail("");
        setPassword("");

    };

    const retrieveUserDetails = (token) => {
        fetch('https://rocky-hollows-82138.herokuapp.com/users/userDetails', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });
        })
    }    

    useEffect(() => {
        if(email !== "" && password !== ""){
            setActive(true);
        } else {
            setActive(false)
        }

    }, [email,password]);

    return (
        (user.id !== null)?
            <Navigate to="/products"/>
         :
        <>
            <Container>
                <h1 className="shadow-sm text-success mt-5 p-3 text-center rounded">Login</h1>
                <Row className="mt-5">
                    <Col lg={5} md={6} sm={12} className="p-5 m-auto shadow-sm rounded-lg">
                        <Form onSubmit ={e => loginUser(e)} >
                            <Form.Group controlId="LogInEmail" className="text-center">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="Enter email" 
                                    required 
                                    value={email} 
                                    onChange = {e => setEmail(e.target.value)} 
                                />
                            </Form.Group>

                            <Form.Group controlId="logInPassword" className="text-center">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Password" 
                                    required
                                    value={password}
                                    onChange = {e => setPassword(e.target.value)}
                                />
                            </Form.Group>
                        { active ?
                            <Button variant="success btn-block mt-3" type="submit" id="logInBtn">
                                Login
                            </Button>
                            :
                            <Button variant="danger btn-block mt-3" type="submit" id="logInBtn">
                                Login
                            </Button>
                        }
                        </Form>
                    </Col>
                </Row>
            </Container>
        </>


	)
}