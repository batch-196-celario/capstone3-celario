import {useState, useEffect} from 'react';
import ProductCard from '../components/ProductCard'



export default function Products() {

	const [products, setProducts] = useState([])

	useEffect(() =>{
		fetch("https://rocky-hollows-82138.herokuapp.com/products/active")
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(products => {
				return (
					<ProductCard key={products._id} productProp = {products}/>
				)
			}))
		})

	}, [])

	return(
		<>
		<h1>Product List:</h1>
		{products}
		</>
	)
}