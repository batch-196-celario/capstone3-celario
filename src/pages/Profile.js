import {useState, useEffect, useContext} from 'react'
import {Navigate, Link} from 'react-router-dom'
import {Button} from 'react-bootstrap'
import profile from '../images/profile.jpg'
import UserContext from '../UserContext';



export default function Profile(){

	const {user, setUser} = useContext(UserContext);

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("")
	const [email, setEmail] = useState("");

	// const retrieveUserDetails = (token) => {
 //        fetch('http://localhost:4000/users/userDetails', {
 //            headers: {
 //                Authorization: `Bearer ${token}`
 //            }
 //        })
 //        .then(res => res.json())
 //        .then(data => {
 //            console.log(data);

 //        })
 //    }

    useEffect(() => {
    	fetch('https://rocky-hollows-82138.herokuapp.com/users/userDetails', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setFirstName(`${data.firstName} ${data.lastName}`)
            setEmail(data.email)
            setMobileNo(data.mobileNo)
        })	
    }, [])

	return(
		<>
		<div id="Profile">
			<div className="profileCard">
				<div className="upper-container">
					<div className="image-container">
						<img src={profile} alt="profile" height="150px" width="150px"/>
					</div>
				</div>
				<div className="lower-container">
					<h3>{firstName}</h3>
					<p>{email}</p>
					<p>{mobileNo}</p>
					{
						(user.isAdmin) ?
						<Button as={Link} to="/dashboard">Go to Dashboard</Button>
						:
						<>
						<Button className="mx-3" disabled>View Orders</Button>
						<Button className="mx-3" disabled>View Cart</Button>
						</>
					}
				</div>
			</div>
		</div>
		</>

	)
}