import {useState, useEffect, useContext} from 'react'
import {Container, Form, Col, Row, Button} from 'react-bootstrap'
import {Navigate, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){
	
	const {user, setUser} =useContext(UserContext);

	const history = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("")
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
 	const [isActive, setIsActive] = useState(false);

 	function userRegister(e){
 		e.preventDefault();

 		fetch('https://rocky-hollows-82138.herokuapp.com/users/checkEmailExists', {
 			method: 'POST',
 			headers: {
 				'Content-Type' : 'application/json'
 			},
 			body: JSON.stringify({
 				email: email
 			})
 		})
 		.then(res => res.json())
 		.then(data => {
 			console.log(data)

 			if(data){
 				Swal.fire({
 					title: "Duplicate email found",
 					icon: "info",
 					text: "email address already exist"
 				});
 			} else {
 				fetch('https://rocky-hollows-82138.herokuapp.com/users', {
 					method: 'POST',
 					headers: {
 						'Content-Type': 'application/json'
 					},
 					body: JSON.stringify({
 						firstName: firstName,
 						lastName: lastName,
 						mobileNo: mobileNo,
 						email: email,
 						password: password
 					})
 				})
 				.then(res => res.json())
 				.then(data => {
 					console.log(data)

 					if(data.email){
 						Swal.fire({
 							title: "Registration successful",
 							icon: "success",
 							text: "Thank you for registering"
 						})
 						history("/login");
 					} else {
 						Swal.fire({
 							title: "Registration Failed",
 							icon: "error",
 							text: "Something went wrong"
 						})
 					}
 				})
 			}
 		})

 		setFirstName("")
 		setLastName("")
 		setMobileNo("")
 		setEmail("")
 		setPassword("")
 	}

 	useEffect(() => {
 		if(email !== "" && firstName !== "" && lastName !== "" && password !== "" && mobileNo !== "" && mobileNo.length === 11){
 			setIsActive(true)
 		} else {
 			setIsActive(false)
 		}
 	}, [firstName, lastName, mobileNo, email, password])



	return (
		(user.id !== null)?
			<Navigate to="/products"/>
		:
		<>
			<Container>
				<h1 className="shadow-sm text-danger mt-2 p-3 text-center rounded">Sign up Now</h1>
				<Row className="mt-2">
					<Col lg={5} md={6} sm={12} className="p-5 m-auto shadow-sm rounded-lg">
						<Form onSubmit ={e => userRegister(e)}>
							<Form.Group className="text-center">
								<Form.Label>First Name</Form.Label>
								<Form.Control
									type= "text"
									placeholder = "First name"
									required
									value = {firstName}
									onChange={e => setFirstName(e.target.value)}
								/>
							</Form.Group>

							<Form.Group className="text-center">
								<Form.Label>Last Name</Form.Label>
								<Form.Control
									type= "text"
									placeholder = "Last name"
									required
									value = {lastName}
									onChange={e => setLastName(e.target.value)}
								/>
							</Form.Group>

							<Form.Group className="text-center">
								<Form.Label>Mobile No.</Form.Label>
								<Form.Control
									type= "text"
									placeholder = "e.g +6399 123 4567"
									required
									value = {mobileNo}
									onChange={e => setMobileNo(e.target.value)}
								/>
							</Form.Group>

							<Form.Group controlId="userEmail" className="text-center">
								<Form.Label>Email Address</Form.Label>
								<Form.Control
									type= "email"
									placeholder = "email address"
									required
									value = {email}
									onChange={e => setEmail(e.target.value)}
								/>
							</Form.Group>

							<Form.Group controlId ="password" className="text-center">
								<Form.Label>Password</Form.Label>
								<Form.Control
									type= "password"
									placeholder = "password"
									required
									value = {password}
									onChange={e => setPassword(e.target.value)}
								/>
							</Form.Group>
						{ isActive ?
							<Button variant="success btn-block mt-3" type="submit" id="logInBtn">
                                Signup
                            </Button>
                            :
     						<Button variant="danger btn-block mt-3" type="submit" id="logInBtn">
                                Signup
                            </Button>
						}
						</Form>
					</Col>
					
				</Row>
			</Container>
		</>

	)
}